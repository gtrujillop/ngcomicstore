(function(){
  var app = angular.module('appUserAccess', ['ngStorage', 'flash']);

  app.factory('userSessionValidator', function(){
    var factory = {};
    factory.isSessionActive = function(sessionData){
      return (sessionData != undefined);
    };
    return factory;
  });

  app.factory('userProfileValidator', function(){
    var factory = {};
    factory.isAdmin = function(user){
      if (user === undefined || user == null) {
        return false
      } else {
        return user.isAdmin;
      }
    }
    return factory;
  });

  app.factory('usersInitializer', function(){
    var factory = {};
    factory.initialize = function(localStorage){
      if (!Array.isArray(localStorage.users)) {
        localStorage.users = [];
        var defaultAdmin = { username: 'gtrujillo',
                          password: '1234567',
                          email: 'gtrujillo@miemail.com',
                          firstName: 'Gaston',
                          lastName: 'Trujillo',
                          isAdmin: true
                        };
        var defaultUser = { username: 'pperez',
                          password: '1234567',
                          email: 'pperez@miemail.com',
                          firstName: 'Pedro',
                          lastName: 'Perez',
                          isAdmin: false,
                          comics: []
                        };
        localStorage.users.push(defaultAdmin);
        localStorage.users.push(defaultUser);
        console.log("User Data initialized.");
      };
    };
    return factory;
  });

  app.directive('userAccess', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/user-access.html',
      controller: function($scope, $sessionStorage, userSessionValidator){
        $scope.isSessionStarted = function($scope){
          return userSessionValidator.isSessionActive($sessionStorage.sessionData);
        };
      }
    }
  })

  app.directive('signIn', function(){

    // TODO Validate users duplication to avoid it
    // TODO Hide sign-in tab when sign in is successful

    return {
      restrict: 'E',
      templateUrl: 'html/templates/sign-in.html',
      controller: function($scope, $localStorage, Flash, usersInitializer){
        $scope.username = '';
        $scope.password = '';
        $scope.email = '';
        $scope.firstName = '';
        $scope.isAdmin = false;
        usersInitializer.initialize($localStorage);
        $scope.addUser = function(){
          var newUser = { username: $scope.username,
                          password: $scope.password,
                          email: $scope.email,
                          firstName: $scope.firstName,
                          lastName: $scope.lastName,
                          isAdmin: $scope.isAdmin,
                          comics: []
                        };
          $localStorage.users.push(newUser);
          Flash.create('success', 'Successfully registered !', 'custom-class');
        };
      }
    };
  })

  app.directive('logIn', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/log-in.html',
      controller: function($scope, $localStorage, $sessionStorage, Flash, usersInitializer){
        $scope.username = '';
        $scope.password = '';
        $scope.user = null;
        $scope.sessionData = { "email": '', "startedOn": 0 };
        usersInitializer.initialize($localStorage);
        $scope.tryAccess = function(){
          $scope.findbyUsernameAndPassw();
          if ($scope.user != null){
            $scope.sessionData.email = $scope.user.email;
            $scope.sessionData.startedOn = Date.now();
            $sessionStorage.sessionData = $scope.sessionData;
            $sessionStorage.user = $scope.user;
            $scope.validAccess = true;
          } else {
            $scope.validAccess = false
          }
          $scope.setAccessAlerts();
          location.reload();
        };
        $scope.setAccessAlerts = function(){
          if($sessionStorage.sessionData === undefined){
            Flash.create('danger', 'Check username/passwod', 'custom-class');
          } else {
            Flash.create('success', 'Log in successful !', 'custom-class');
          }
        }
        $scope.findbyUsernameAndPassw = function(){
          var data = $localStorage.users;
          for(var i = 0; i < data.length; i++ ){
            if(data[i].username == $scope.username && data[i].password == $scope.password){
              $scope.user = data[i];
              break;
            }
          }
        }
      }
    };
  })

  app.directive('userSession', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/user-session.html',
      controller: function($scope, $sessionStorage, $localStorage, userSessionValidator){
        $scope.user = $sessionStorage.user;
        $scope.isSessionStarted = function(){
          return userSessionValidator.isSessionActive($sessionStorage.sessionData);
        }
        $scope.destroySession = function() {
          var index = $scope.findUserbyEmailIndex($scope.user.email);
          console.log(index);
          if ($sessionStorage.user.comics != []){
            $localStorage.users[index] = $sessionStorage.user;
          }
          $sessionStorage.$reset();
        }
        $scope.findUserbyEmailIndex = function(email) {
          var index = 0;
          for (var i = 0; i < $localStorage.users.length; i ++) {
            if($localStorage.users[i].email === email){
              index = i;
              break;
            }
          }
          return index;
        }
      }
    };
  });

  app.directive('userOptions', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/user-options.html',
      controller: function($scope, $sessionStorage, userSessionValidator, Flash, userProfileValidator){
        $scope.user = $sessionStorage.user;
        $scope.hasAdminRole = userProfileValidator.isAdmin($scope.user);
        // TODO extract this out as a global function
        $scope.isSessionStarted = function(){
          return userSessionValidator.isSessionActive($sessionStorage.sessionData);
        }
      }
    };
  })

  app.directive('userProfile', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/user-profile.html',
      controller: function($scope, $sessionStorage){
        $scope.enableProfile = false;
        $scope.user = $sessionStorage.user;
        $scope.toggleProfile = function(){
          $scope.enableProfile = !$scope.enableProfile;

        }
        $scope.hideProfile = function(){
          $scope.enableProfile = false;
        }
      }
    }
  })
  app.directive('userComics', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/user-comics.html',
      controller: function($scope, $sessionStorage){
        $scope.enableComics = false;
        $scope.user = $sessionStorage.user;
        $scope.toggleComics = function(){
          if($sessionStorage.user.comics.length != 0){
            $scope.enableComics = !$scope.enableComics;
            $scope.findUserComics();
          }
        }
        $scope.hideComics = function(){
          $scope.enableComics = false;
        }
        $scope.findUserComics = function(){
          $scope.Mycomics = $sessionStorage.user.comics;
        }
      }
    }
  })

})();
