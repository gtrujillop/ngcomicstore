(function(){
  var app = angular.module('appComicOperations', ['ngDialog']);

  app.value('comicIndexUrl', 'assets/json/comics.json');

  app.directive('comicsIndex', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/comics-index.html',
      controller: function($scope, $http, $sessionStorage, comicIndexUrl, userSessionValidator, ngDialog){
        $scope.comics = [];
        $scope.comic = null;
        $scope.showModal = false;
        $http.get(comicIndexUrl)
          .success(function(data) {
            $scope.comics = data;
        });
        $scope.isSessionStarted = function(){
          return userSessionValidator.isSessionActive($sessionStorage.sessionData);
        };
        $scope.addComicToUser = function(){
          var user = $sessionStorage.user;
          $sessionStorage.user.comics.push($scope.comic);
        }
        $scope.showComic = function(comic){
          $scope.comic = comic;
          ngDialog.open({ template: 'html/templates/show-comic.html',
                          scope: $scope
          });
        }
      }
    };
  })

  app.directive('comicRatings', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/comic-ratings.html',
      controller: function($scope, $http, comicIndexUrl){
        $scope.comics = [];
        $scope.getComics = function() {
          $http.get(comicIndexUrl)
            .success(function(data) {
              $scope.processComicRatings(data);
          });
        }
        $scope.processComicRatings = function(comics) {
          var ratingAcum = 0;
          var ratingCount = 0;
          for(var i = 0; i < comics.length; i ++) {
            var comic = comics[i];
            ratingAcum = 0;
            ratingCount = 0;
            if(comic.ratings.length != 0) {
              for(var j = 0; j < comic.ratings.length; j ++) {
                ratingAcum += comic.ratings[j].rate;
                ratingCount += 1;
              };
            }
            comic.ratingsAvg = ratingAcum / ratingCount;
          }
          $scope.comics = comics;
        }
      }
    };
  })
  app.directive('comicViews', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/comic-views.html',
      controller: function($scope, $http, comicIndexUrl){
        $scope.comics = [];
        $scope.getComics = function() {
          $http.get(comicIndexUrl)
            .success(function(data) {
              $scope.comics = data;
          });
        }
      }
    };
  })

  app.directive('comicSearch', function(){
    return {
      restrict: 'E',
      templateUrl: 'html/templates/comic-search.html',
      controller: function($scope, $sessionStorage){
        $scope.enableSearch = false;
        $scope.toggleSearch = function(){
          $scope.enableSearch = !$scope.enableSearch;

        }
        $scope.hideSearch = function(){
          $scope.enableSearch = false;
        }
      }
    }
  })
})();
