(function(){

  // TODO split elements in files per functionallity
  //module
  var app = angular.module('App', ['ngStorage', 'flash',
                                  'appUserAccess', 'appComicOperations'
                                  ]);

  app.controller('TabsController', function($scope){
    $scope.tab = 0;
    $scope.setActiveTab = function(tabValue){
      $scope.tab = tabValue;
    };
    $scope.isCurrentTab = function(currentTabValue){
      return $scope.tab === currentTabValue;
    };
  });

  app.directive('ngConfirmClick', function () {
    return {
      priority: 1,
      terminal: true,
      link: function (scope, element, attr) {
        var msg = attr.confirmationNeeded || "Do you want to purchase this comic ?";
        var clickAction = attr.ngClick;
        element.bind('click',function () {
          if ( window.confirm(msg) ) {
            scope.$eval(clickAction)
          }
        });
      }
    };
  });

})();
